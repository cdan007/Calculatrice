/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inclusiv33
 */
public class CalculatriceTest {
    
    
     public CalculatriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        double nombre1 = 2.0;
        double nombre2 = 4.0;
        double expResult = 6.0;
        double result = Calculatrice.addition(nombre1, nombre2);
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        double nombre1 = 3.0;
        double nombre2 = 2.0;
        double expResult = 1.0;
        double result = Calculatrice.soustraction(nombre1, nombre2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        double multiplicateur = 2.0;
        double multiplicande = 5.0;
        double expResult = 10.0;
        double result = Calculatrice.multiplication(multiplicateur, multiplicande);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double dividende = 4.0;
        double diviseur = 0.0;
        double expResult = 0.0;
        double result = Calculatrice.division(dividende, diviseur);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of moyenne method, of class Calculatrice.
     */
    @Test
    public void testMoyenne() {
        System.out.println("moyenne");
        double[] nombres = {2.0, 15.0 , 1.0};
        double expResult = 6.0;
        double result = Calculatrice.moyenne(nombres);
        assertEquals(expResult, result, 0.0);
    }
  
}
