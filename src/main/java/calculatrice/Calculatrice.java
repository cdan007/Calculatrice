/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import java.util.Scanner;

/**
 *
 * @author dinas
 */
public class Calculatrice {
    
    public static double addition (double nombre1, double nombre2){
        return nombre1 + nombre2;
    }
    
    public static double soustraction (double nombre1, double nombre2){
        return nombre1 - nombre2;
    }
    
    public static double multiplication (double multiplicateur, double multiplicande){
        return multiplicande * multiplicateur;
    }
    
    public static double division (double dividende, double diviseur){
        if (diviseur == 0){
            return 0.0;
        }
        
        return dividende / diviseur;
    }
    
    public static double moyenne (double[] nombres){
        double total = 0;
        for (double nombre:nombres){
            total = addition(total, nombre);
        }
        
        return division(total, nombres.length);
    }
}
